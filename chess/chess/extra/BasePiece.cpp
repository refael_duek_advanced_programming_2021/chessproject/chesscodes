#include "BasePiece.h"



BasePiece::BasePiece(int player, char type) :
    _player(player), _type(type)
{
}

BasePiece::~BasePiece()
{
}


int BasePiece::index_in_board(char place, int place_in_line)
{
    //the numer of line is in ascii
    if (place_in_line == 49)
    {
        place_in_line = 1;
    }
    else if (place_in_line == 50)
    {
        place_in_line = 2;
    }
    else if (place_in_line == 51)
    {
        place_in_line = 3;
    }
    else if (place_in_line == 52)
    {
        place_in_line = 4;
    }
    else if (place_in_line == 53)
    {
        place_in_line = 5;
    }
    else if (place_in_line == 54)
    {
        place_in_line = 6;
    }
    else if (place_in_line == 55)
    {
        place_in_line = 7;
    }
    else if (place_in_line == 56)
    {
        place_in_line = 8;
    }

    int check = (8 - place_in_line) * 8;
    int by_num;

    if (place == 'a')
    {
        by_num = 0;
    }
    else if (place == 'b')
    {
        by_num = 1;
    }
    else if (place == 'c')
    {
        by_num = 2;
    }
    else if (place == 'd')
    {
        by_num = 3;
    }
    else if (place == 'e')
    {
        by_num = 4;
    }
    else if (place == 'f')
    {
        by_num = 5;
    }
    else if (place == 'g')
    {
        by_num = 6;
    }
    else if (place == 'h')
    {
        by_num = 7;
    }
    return check + by_num;
}

/*will check if the player can be on a place in the board
* to_put: the current place
* dst_place: the target place
* i: to check if the piece is in his last place
* board: the board
*/
bool BasePiece::by_player(int to_put, char dst_place, int i, std::string board)
{
    if (_player == 0)//if the player is white 
    {
        if (std::isupper(board[to_put]) && i == int(dst_place))//if the player try to eat is own piece return false
        {
            return false;
        }
        else if (board[to_put] != '#' && i != int(dst_place))
        {
            return false;
        }
    }
    else if (_player != 0)//if the player is black
    {
        if (std::islower(board[to_put]) && i == int(dst_place))//if the player try to eat is own piece return false
        {
            return false;
        }
        else if (board[to_put] != '#' && i != int(dst_place))
        {
            return false;
        }
    }
    return true;
}

char BasePiece::get_type()
{
    return this->_type;
}

int BasePiece::get_player()
{
    return this->_player;
}
