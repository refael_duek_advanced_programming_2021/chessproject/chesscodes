#pragma once
#include <string>
#include <iostream>
#include "BasePiece.h"


class Bishop : public BasePiece
{
public:
    Bishop(int player, char type);
    ~Bishop();

    virtual bool good_place(std::string msg, std::string board);//will check if the tool can move to the new place on the board


};