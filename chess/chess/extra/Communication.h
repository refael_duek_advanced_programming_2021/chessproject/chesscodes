#pragma once
#include <string>
#include <iostream>
#include <thread>
#include "Pipe.h"

using std::cout;
using std::endl;
using std::string;

class Communication
{
	Pipe _p;
public:

	Communication();
	~Communication();

	void send_board();

	void send_msg(char msg[]);//will send a string to the frontend

	std::string get_msg();//will get the play msg("e2e3")

	void start_pipe();

};