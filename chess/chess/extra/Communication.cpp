#include "Communication.h"


Communication::Communication()
{
	start_pipe();
}

Communication::~Communication()
{
	_p.close();
}


void Communication::send_msg(char msg[])
{
	_p.sendMessageToGraphics(msg);   // send the board string
}

std::string Communication::get_msg()
{
	//get message from graphics
	string msgFromGraphics = _p.getMessageFromGraphics();
	return msgFromGraphics;
}



void Communication::send_board()
{
	char msg[1024];

	strcpy_s(msg, "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0");

	_p.sendMessageToGraphics(msg);
}

void Communication::start_pipe()
{
	srand(time_t(NULL));
	bool isConnect = _p.connect();
	string ans = "";
	while (!isConnect)
	{
		
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, alse-exit)" << endl;
		std::cin >> ans;
		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = _p.connect();
		}
		else
		{
			cout << "good bye" << endl;
			exit(1);
		}
	}
}
