#pragma once
#include <string>
#include <iostream>


class BasePiece
{
protected:
    int _player;
    char _type;

public:
    BasePiece(int player, char type);
    virtual ~BasePiece();

    virtual bool good_place(std::string const msg , std::string board) = 0;//will check if the tool can move to the new place on the board
    
    //helper func
    int index_in_board(char place, int place_in_line);//will get the index of a place in the board
    bool by_player(int to_put, char dst_place, int i, std::string board);//will check if the player can be on a place in the board
    //getters
    char get_type();
    int get_player();

};