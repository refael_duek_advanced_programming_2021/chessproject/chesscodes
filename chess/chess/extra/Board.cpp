#include "Board.h"


Board::Board(std::string board)
{
    this->_turn = 0;
    int place_in_board = 0;

    for (int i = 0; i < BOARD_SIZE; i++)
    {
        for (int j = 0; j < BOARD_SIZE; j++)
        {
            if (board[place_in_board] == 'k')//black
            {
                this->_board[i][j] = new King(BLACK_NUM, 'k');
            }
            else if (board[place_in_board] == 'K')//white
            {
                this->_board[i][j] = new King(WHITE_NUM, 'K');
            }
            else if (board[place_in_board] == 'r')//black
            {
                this->_board[i][j] = new Rook(BLACK_NUM, 'r');
            }
            else if (board[place_in_board] == 'R')//white
            {
                this->_board[i][j] = new Rook(WHITE_NUM, 'R');
            }
            else if (board[place_in_board] == 'B')//white
            {
                this->_board[i][j] = new Bishop(WHITE_NUM, 'B');
            }
            else if (board[place_in_board] == 'b')//black
            {
                this->_board[i][j] = new Bishop(BLACK_NUM, 'b');
            }
            else if (board[place_in_board] == 'Q')//white
            {
                this->_board[i][j] = new Queen(WHITE_NUM, 'Q');
            }
            else if (board[place_in_board] == 'q')//black
            {
                this->_board[i][j] = new Queen(BLACK_NUM, 'q');
            }
            else if (board[place_in_board] == 'N')//white
            {
                this->_board[i][j] = new Knight(WHITE_NUM, 'N');
            }
            else if (board[place_in_board] == 'n')//black
            {
                this->_board[i][j] = new Knight(BLACK_NUM, 'n');
            }        
            else if (board[place_in_board] == 'P')//white
            {
                this->_board[i][j] = new Pawn(WHITE_NUM, 'P');
            }
            else if (board[place_in_board] == 'p')//black
            {
                this->_board[i][j] = new Pawn(BLACK_NUM, 'p');
            }
            else//blank
            {
                _board[i][j] = nullptr;
            }
            place_in_board++;
        }
    }
}

Board::~Board()
{
    for (int i = 0; i < BOARD_SIZE; i++)
    {
        for (int j = 0; j < BOARD_SIZE; j++)
        {
            delete _board[i][j];
        }
    }
}

void Board::set_move(std::string msg)
{
    //current index
    int curr_num = 8 - (int(msg[1]) - 49) - 1;//[*][]
    int curr_letter = int(msg[0]) - 97;   //[][*]

    //dst index
    int dst_num = 8 - (int(msg[3]) - 49) - 1;//[*][]
    int dst_letter = int(msg[2]) - 97;   //[][*]

    if (this->_board[curr_num][curr_letter]->get_type() == 'k')//black king
    {
        delete this->_board[curr_num][curr_letter];
        this->_board[curr_num][curr_letter] = nullptr;
        delete this->_board[dst_num][dst_letter];
        this->_board[dst_num][dst_letter] = new King(1, 'k');
    }
    else if (this->_board[curr_num][curr_letter]->get_type() == 'K')//white king
    {
        delete this->_board[curr_num][curr_letter];
        this->_board[curr_num][curr_letter] = nullptr;
        delete this->_board[dst_num][dst_letter];
        this->_board[dst_num][dst_letter] = new King(0, 'K');
    }
    else if (this->_board[curr_num][curr_letter]->get_type() == 'r')//black Rook
    {
        delete this->_board[curr_num][curr_letter];
        this->_board[curr_num][curr_letter] = nullptr;
        delete this->_board[dst_num][dst_letter];
        this->_board[dst_num][dst_letter] = new Rook(1, 'r');
    }
    else if (this->_board[curr_num][curr_letter]->get_type() == 'R')//white Rook
    {
        delete this->_board[curr_num][curr_letter];
        this->_board[curr_num][curr_letter] = nullptr;
        delete this->_board[dst_num][dst_letter];
        this->_board[dst_num][dst_letter] = new Rook(0, 'R');
    }
    else if (this->_board[curr_num][curr_letter]->get_type() == 'b')//black Bishop
    {
        delete this->_board[curr_num][curr_letter];
        this->_board[curr_num][curr_letter] = nullptr;
        delete this->_board[dst_num][dst_letter];
        this->_board[dst_num][dst_letter] = new Bishop(1, 'b');
    }
    else if (this->_board[curr_num][curr_letter]->get_type() == 'B')//white Bishop
    {
        delete this->_board[curr_num][curr_letter];
        this->_board[curr_num][curr_letter] = nullptr;
        delete this->_board[dst_num][dst_letter];
        this->_board[dst_num][dst_letter] = new Bishop(0, 'B');
    }
    else if (this->_board[curr_num][curr_letter]->get_type() == 'q')//black Queen
    {
        delete this->_board[curr_num][curr_letter];
        this->_board[curr_num][curr_letter] = nullptr;
        delete this->_board[dst_num][dst_letter];
        this->_board[dst_num][dst_letter] = new Queen(1, 'q');
    }
    else if (this->_board[curr_num][curr_letter]->get_type() == 'Q')//white Queen
    {
        delete this->_board[curr_num][curr_letter];
        this->_board[curr_num][curr_letter] = nullptr;
        delete this->_board[dst_num][dst_letter];
        this->_board[dst_num][dst_letter] = new Queen(0, 'Q');
    }
    else if (this->_board[curr_num][curr_letter]->get_type() == 'n')//black Knight
    {
        delete this->_board[curr_num][curr_letter];
        this->_board[curr_num][curr_letter] = nullptr;
        delete this->_board[dst_num][dst_letter];
        this->_board[dst_num][dst_letter] = new Knight(1, 'n');
    }
    else if (this->_board[curr_num][curr_letter]->get_type() == 'N')//white Knight
    {
        delete this->_board[curr_num][curr_letter];
        this->_board[curr_num][curr_letter] = nullptr;
        delete this->_board[dst_num][dst_letter];
        this->_board[dst_num][dst_letter] = new Knight(0, 'N');
    }
    
    else if (this->_board[curr_num][curr_letter]->get_type() == 'p')//black Pawn
    {
        delete this->_board[curr_num][curr_letter];
        this->_board[curr_num][curr_letter] = nullptr;
        delete this->_board[dst_num][dst_letter];
        this->_board[dst_num][dst_letter] = new Pawn(1, 'p');
    }
    else if (this->_board[curr_num][curr_letter]->get_type() == 'P')//white Pawn
    {
        delete this->_board[curr_num][curr_letter];
        this->_board[curr_num][curr_letter] = nullptr;
        delete this->_board[dst_num][dst_letter];
        this->_board[dst_num][dst_letter] = new Pawn(0, 'P');
    }

}


int Board::check_all_ifs(std::string msg)
{
    int checker = -1;  //start from -1,
    std::string save_board_string = ""; //save the board in string

    int curr_num = 8 - (int(msg[1]) - 49) - 1;
    int curr_letter = int(msg[0]) - 97;   

    //getting the board as string
    save_board_string = make_board_as_string();
    /*------------------------------------------------------*/
    //checking the second thing
    checker = is_start_index(msg, save_board_string);

    //means that the return from the sec function is false
    if (checker == 0 || _board[curr_num][curr_letter] == nullptr)
    {
        return 2; //return the num 2 
    }

    /*------------------------------------------------------*/
    //checking the 4 thing
    checker = is_making_shah(msg, this->_turn);
    if (checker == 1)
    {
        return 4;
    }
    /*------------------------------------------------------*/
    //checking the third thing
    checker = is_my_player(msg, save_board_string);

    //means that the return from the third function is false
    if (checker == 0)
    {
        return 3; //return the num 3
    }
    /*------------------------------------------------------*/
    //checking the first thing
    //check if a player make a shah on the other player
    Board temp = Board(this->make_board_as_string());
    checker = _board[curr_num][curr_letter]->good_place(msg, save_board_string);
    if (checker == 1)
    {
        temp.set_move(msg);
        King k = King(0, 'k');
        checker = k.check_if_shah(msg, temp.make_board_as_string(), this->_turn);
        if (checker == 1)
        {
            this->update_turn();
            this->set_move(msg);

            return 1;
        }
    }
    /*------------------------------------------------------*/
    //checking the five thing
    checker = is_out_of_board(msg);
    if (checker == 0)
    {
        return 5; //return the num 5
    }
    /*------------------------------------------------------*/

    //checking the six thing
    //current index
    checker = _board[curr_num][curr_letter]->good_place(msg, save_board_string);
    if (checker == 0)
    {
        return 6; //return the num 6
    }
    /*------------------------------------------------------*/

    //checking the seven thing
    checker = isTheSame(msg);
    if (checker == 0)
    {
        return 7; //return the num 7
    }
    /*------------------------------------------------------*/

    //good move
    this->update_turn();
    this->set_move(msg);
    return 0;
}

void Board::update_turn()
{
    if (this->_turn == 0)
    {
        this->_turn = 1;
    }
    else
    {
        this->_turn = 0;
    }
}

int Board::get_turn()
{
    return this->_turn;
}



bool Board::is_start_index(std::string msg, std::string board)
{
    King temp = King(0, 'k');
    int start_index = temp.index_in_board(msg[0], int(msg[1]));

    if (this->_turn == 0)//white
    {
        //if in the white turn the start index is lower letter this is not the white piece
        if (islower(board[start_index]))
        {
            return false;
        }
    }
    else//black
    {
        //if in the black turn the start index is upper letter this is not the black piece
        if (!islower(board[start_index]) )
        {
            return false;
        }
    }

    return true;
}

bool Board::is_my_player(std::string msg, std::string board)
{
    char dst_letter = msg[2];
    int dst_num = msg[3];//in ascii
    
    //for dst index
    King temp = King(0, 'k');
    int dst_index = temp.index_in_board(dst_letter, dst_num);

    if (board[dst_index] == '#')
    {
        return true;
    }

    if (this->_turn == 0)//white
    {
        //if the place letter is an upper one white can't go to this dst 
        if (!islower(board[dst_index]))
        {
            return false;
        }
    }
    else//black
    {
        //if the place letter is an lower one black can't go to this dst 
        if (islower(board[dst_index]))
        {
            return false;
        }
    }

    return true;
}

//the board need to be between 'A'-'H' , 1-8
//if the msg is "e2e4" we need to check the places
bool Board::is_out_of_board(std::string msg)
{
    
    //dst
    char save_dst_char = msg[2];
    int save_dst_num = msg[3];
    //49 = 1 on ascii and 56 = 8 on ascii
    if ((save_dst_char < 'a'  || save_dst_char > 'h') ||(save_dst_num < 49 || save_dst_num > 56))
    {
        return false;
    }
    
    //current 
    char save_curr_char = msg[0];
    int save_curr_num = msg[1];
    //49 = 1 on ascii and 56 = 8 on ascii
    if ((save_curr_char < 'a' || save_curr_char > 'h') || (save_curr_num < 49 || save_curr_num > 56))
    {
        return false;
    }

    return true;
}

bool Board::isTheSame(std::string msg)
{
    //current 
    char save_curr_char = msg[0];
    int save_curr_num = msg[1];
    //dst
    char save_dst_char = msg[2];
    int save_dst_num = msg[3];

    //checking if they both same
    if (save_curr_char == save_dst_char)
    {
        if (save_curr_num == save_dst_num)
        {
            return false;
        }
    }
    return true;
}

bool Board::is_making_shah(std::string msg, int who)
{
    int check;
    int curr_num = 8 - (int(msg[1]) - 49) - 1;
    int curr_letter = int(msg[0]) - 97;
    Board temp = Board(this->make_board_as_string());//will do the checking on him
    if (who == 1)//if this is the black turn
    {
        temp.update_turn();
    }  
    check = _board[curr_num][curr_letter]->good_place(msg, temp.make_board_as_string());
    if (check == 1)
    {
        temp.set_move(msg);
        King k = King(0, 'k');

        int king_index = temp.find_king_index_on_board(!temp._turn);

        if (temp.is_possible_to_eat(king_index / 10, king_index % 10, !temp._turn))//ij-> ij/10 = i------ij % 10 = j
        {
            return true;
        }
        return false;
    }
}

int Board::make_index_for_board(char first)
{
    if (first == 'a')
    {
        return 0;
    }
    else if (first == 'b')
    {
        return 1;
    }
    else if (first == 'c')
    {
        return 2;
    }
    else if (first == 'd')
    {
        return 3;
    }
    else if (first == 'e')
    {
        return 4;
    }
    else if (first == 'f')
    {
        return 5;
    }
    else if (first == 'g')
    {
        return 6;
    }
    else if (first == 'h')
    {
        return 7;
    }
}

std::string Board::make_board_as_string()
{
    std::string board = "";
    for (int i = 0; i < BOARD_SIZE; i++)
    {
        for (int j = 0; j < BOARD_SIZE; j++)
        {
            if (this->_board[i][j] != nullptr)
            {
                board += this->_board[i][j]->get_type();
            }
            else
            {
                board += "#";
            }
        }
    }


    return board;
}

//will check if can eat the i and j pos
bool Board::is_possible_to_eat(int i_index, int j_index, int who)//7,7
{
    //char(97-j)->letter curr
    //char(56-i)->number curr
    //char(97-j_index)->letter dst
    //char(56-i_index)->number dst
    char letter_dst = 97 + j_index;
    char number_dst = 56 - i_index;
    
    std::string my_board = this->make_board_as_string();

    std::string msg = "";
    for (int i = 0; i < BOARD_SIZE; i++)
    {
        for (int j = 0; j < BOARD_SIZE; j++)
        {
            if (this->_board[i][j] != nullptr)//if have something in this place
            {
                msg = "";
                if (this->_board[i][j]->get_player() == who)
                {
                    msg += char(97 + j);
                    msg += char(56 - i);
                    msg += letter_dst;
                    msg += number_dst;
                    //if can eat the king will return true
                    if (this->_board[i][j]->good_place(msg, my_board))
                    {
                        return true;
                    }
                }
            }
        }
    }
    return false;
}


//will return the index of a king/ the return will be "ij"
int Board::find_king_index_on_board(int who)
{
    for (int i = 0; i < BOARD_SIZE; i++)
    {
        for (int j = 0; j < BOARD_SIZE; j++)
        {
            if (_board[i][j] != nullptr)
            {
                if (who == 0)//"K"
                {
                    if (_board[i][j]->get_type() == 'k')
                    {
                        return i * 10 + j;
                    }
                }
                else//'k'
                {
                    if (_board[i][j]->get_type() == 'K')
                    {
                        return i * 10 + j;
                    }
                }
            }
            
        }
    }
    return 0;
}
