#include "King.h"

King::King(int player, char type) : BasePiece(player, type)
{

}

King::~King()
{
}

bool King::good_place(std::string msg, std::string board)
{

    if (msg[3] - msg[1] == 1 || msg[3] - msg[1] == -1 || msg[3] - msg[1] == 0)
    {
        if (msg[0] - msg[2] == 1 || msg[0] - msg[2] == -1 || msg[0] - msg[2] == 0)
        {
            
            int my_place = index_in_board(msg[2], msg[3]);
            char dst_place = msg[3];
            return by_player(my_place, dst_place, dst_place, board);
        }
    }
    return false;
}


//msg is the move, board is the main board and turn is black or white turn
bool King::check_if_shah(std::string msg, std::string board, int turn)
{
    Board temp = Board(board);
    int king_index = temp.find_king_index_on_board(turn);

    if (turn != 0)//if this is not the white turn need to put the black turn
    {
        temp.update_turn();
    }

    if (temp.is_possible_to_eat(king_index / 10, king_index % 10, turn))//ij-> ij/10 = i------ij % 10 = j
    {
        return true;
    }
    return false;
}

