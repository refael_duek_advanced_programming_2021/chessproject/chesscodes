#include "Knight.h"

Knight::Knight(int player, char type) : BasePiece(player, type)
{
}

Knight::~Knight()
{
}


bool Knight::good_place(std::string msg, std::string board)
{
	if (msg[0] - msg[2] == 2 || msg[0] - msg[2] == -2)
	{
		if (msg[1] - msg[3] == 1 || msg[1] - msg[3] == -1)
		{
			return by_player(index_in_board(msg[2], msg[3]), msg[3], msg[3], board);
		}
	}
	else if (msg[0] - msg[2] == -1 || msg[0] - msg[2] == 1)
	{
		if (msg[1] - msg[3] == 2 || msg[1] - msg[3] == -2)
		{
			return by_player(index_in_board(msg[2], msg[3]) , msg[3], msg[3], board);
		}
	}
	return false;
}
