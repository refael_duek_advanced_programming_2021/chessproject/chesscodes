#pragma once
#include <string>
#include <iostream>
#include "BasePiece.h"


class Pawn : public BasePiece
{
public:
    Pawn(int player, char type);
    ~Pawn();

    virtual bool good_place(std::string msg, std::string board);//will check if the tool can move to the new place on the board


};