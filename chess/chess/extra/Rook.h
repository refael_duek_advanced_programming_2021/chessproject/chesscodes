#pragma once
#include <string>
#include <iostream>
#include "BasePiece.h"


class Rook : public BasePiece
{
public:
    Rook(int player, char type);
    ~Rook();

    virtual bool good_place(std::string msg, std::string board);//will check if the tool can move to the new place on the board
    
    
};