#pragma once
#include <string>
#include <iostream>
#include "BasePiece.h"
#include "King.h"
#include "Rook.h"
#include "Bishop.h"
#include "Queen.h"
#include "Knight.h"
#include "Pawn.h"

#define BOARD_SIZE 8
#define WHITE_NUM 0
#define BLACK_NUM 1

class Board
{
private:
    BasePiece* _board[BOARD_SIZE][BOARD_SIZE];
    int _turn;

public:
    Board(std::string board);
    ~Board();

    void set_move(std::string msg);//will do the move (e2 -> d3)

    int check_all_ifs(std::string msg);//will update the board, will use the msg from the communication and will the index of the front msg(the 0-8 msg)

    void update_turn();//will change the turn
    int get_turn();


    bool is_start_index(std::string msg, std::string board);//will check if in the start index have the current player piece, code number 2
    bool is_my_player(std::string msg, std::string board);//will check if in the dst place there is the same player piece, code number 3
    bool is_out_of_board(std::string msg); //will check if the msg is in the board, code number 5
    bool isTheSame(std::string msg); //will check if the current place and the dst are the same, code number 7
    bool is_making_shah(std::string msg, int who);//will check if make a shah if a piece move, code number 4
     
    //helper func
    int make_index_for_board(char first);
    std::string make_board_as_string();

    bool is_possible_to_eat(int i_index, int j_index, int who);//will check if can eat the i and j pos
    int find_king_index_on_board(int who);//will return the index of a king/ the return will be "ij"
};