#pragma once
#include <string>
#include <iostream>
#include "BasePiece.h"


class Knight : public BasePiece
{
public:
    Knight(int player, char type);
    ~Knight();

    virtual bool good_place(std::string msg, std::string board);//will check if the tool can move to the new place on the board


};