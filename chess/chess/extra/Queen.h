#pragma once
#include <string>
#include <iostream>
#include "BasePiece.h"
#include "Rook.h"
#include "Bishop.h"

//Combination of rook and bishop
class Queen : public BasePiece
{
    
public:
    Queen(int player, char type);
    ~Queen();

    virtual bool good_place(std::string msg, std::string board);//will check if the tool can move to the new place on the board
    
    bool good_for_black(std::string msg, std::string board);//will do the checking if the piece is black
    bool good_for_white(std::string msg, std::string board);//will do the checking if the piece is white

};