#pragma once
#include <string>
#include <iostream>
#include "BasePiece.h"

#include "Board.h"


class King : public BasePiece
{
    
public:
    King(int player, char type);
    ~King();
    
    virtual bool good_place(std::string msg, std::string board);//will check if the tool can move to the new place on the board
    bool check_if_shah(std::string msg, std::string board, int turn);//will check if shah happened
};
