#include "Menu.h"

void Menu::start()
{
	Board my_board = Board(BOARD);
	Communication comm = Communication();//start the pipe
	comm.send_board();

	std::string msgFromGraphics = comm.get_msg();
	while (msgFromGraphics != "quit")
	{
		// should handle the string the sent from graphics
		// according the protocol. Ex: e2e4           (move e2 to e4)
		int king_of_msg = my_board.check_all_ifs(msgFromGraphics);
		std::cout << std::endl;
		std::cout << std::endl;
		//std::cout << my_board.make_board_as_string() << std::endl;
		std::cout << std::endl;
		std::cout << std::endl;
		
		// YOUR CODE

		char msgToGraphics[1024];//?
		strcpy_s(msgToGraphics, "YOUR CODE"); // msgToGraphics should contain the result of the operation
		
		/******* JUST FOR EREZ DEBUGGING ******/
		int r = rand() % 10; // just for debugging......
		msgToGraphics[0] = (char)(king_of_msg + '0');
		msgToGraphics[1] = 0;
		/******* JUST FOR EREZ DEBUGGING ******/
		
		
		// return result to graphics
		comm.send_msg(msgToGraphics);
		
		// get message from graphics
		msgFromGraphics = comm.get_msg();
	}
}

