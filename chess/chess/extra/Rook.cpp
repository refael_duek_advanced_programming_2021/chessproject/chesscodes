#include "Rook.h"

Rook::Rook(int player, char type) : BasePiece(player, type)
{
}

Rook::~Rook()
{
}




bool Rook::good_place(std::string msg, std::string board)
{
	int to_put = 0;
	bool flag = true;
	//will check the places
	if ((msg[0] == msg[2]) || (msg[1] == msg[3]))//if the msg is good input
	{
		if (msg[0] == msg[2])//if the column is the same
		{
			char my_place = msg[1];
			char dst_place = msg[3];


			if (my_place < dst_place)//will do the checking if the piece place is bigger than the dst place
			{
				for (int i = int(my_place) + 1; i <= int(dst_place); i++)
				{
					//will check if the index in the board is blank
					to_put = index_in_board(msg[0], i);
					flag = by_player(to_put, dst_place, i, board);
					if (flag == false)
					{
						return false;
					}
				}
			}
			else if (my_place > dst_place)//will do the checking if the piece place is lower than the dst place
			{
				for (int i = int(my_place) - 1; i >= int(dst_place); i--)
				{
					//will check if the index in the board is blank
					to_put = index_in_board(msg[0], i);
					flag = by_player(to_put, dst_place, i, board);
					if (flag == false)
					{
						return false;
					}
				}
			}
		}


		if (msg[1] == msg[3])//if the line is the same
		{
			char my_place = msg[0];
			char dst_place = msg[2];

			if (my_place < dst_place)//will do the checking if the piece place is bigger than the dst place
			{
				for (int i = int(my_place) + 1; i <= int(dst_place); i++)//2->5
				{
					//will check if the index in the board is blank
					to_put = index_in_board(char(i), int(msg[1]));
					flag = by_player(to_put, dst_place, i, board);
					if (flag == false)
					{
						return false;
					}
				}
			}
			else if (my_place > dst_place)//will do the checking if the piece place is lower than the dst place
			{
				for (int i = int(my_place) - 1; i >= int(dst_place); i--)
				{
					//will check if the index in the board is blank
					to_put = index_in_board(char(i), int(msg[1]));
					flag = by_player(to_put, dst_place, i, board);
					if (flag == false)
					{
						return false;
					}
				}
			}
		}
	}
	else
	{
		return false;
	}
	return flag;
}
