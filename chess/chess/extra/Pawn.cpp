#include "Pawn.h"

Pawn::Pawn(int player, char type) : BasePiece(player, type)
{
}

Pawn::~Pawn()
{
}

bool Pawn::good_place(std::string msg, std::string board)
{
	int index;
	int curr_num = msg[1];
	char curr_letter = msg[0];
	int num;
	char letter;
	bool ans = false;

	//will check if this is the first time we move with the this pawn
	int first = 1;
	if (msg[1] == 55 || msg[1] == 50)
	{
		first = 0;
	}

	int turn = -1;
	if (this->_player == 0)
	{
		turn = 1;
	}


	
	//if try to move 1 place
	if (curr_num + (turn * 1) == msg[3] && curr_letter == msg[2])
	{
		index = index_in_board(msg[2], msg[3]);
		if (board[index] == '#')//good
		{
			return true;			
		}
		else
		{
			return false;
		}
	}
	//if try to move 2 places
	else if (curr_num + (turn * 2) == msg[3] && curr_letter == msg[2])
	{
		if (first != 0)//if not first time can't move 2 places
		{
			return false;
		}
		else
		{
			num = curr_num + (turn * 1);
			letter = curr_letter;
			index = index_in_board(msg[2], msg[3]);
			if (board[index_in_board(msg[2], msg[3])] == '#' && board[index_in_board(letter, num)] == '#')
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	//check eating rigth
	if (msg[0] + 1 == msg[2] && msg[1] + (turn * 1) == msg[3])
	{
		index = index_in_board(msg[2], msg[3]);
		if (this->_player == 0 && (isupper(board[index]) == true || board[index] == '#'))
		{
			return false;
		}
		else if(this->_player == 0 && isupper(board[index]) != true)
		{
			return true;
		}
		else if (this->_player == 1 && isupper(board[index]) != true)
		{
			return false;
		}
		else if (this->_player == 1 && isupper(board[index]) == true)
		{
			return true;
		}
	}

	//check eating left
	if (msg[0] - 1 == msg[2] && msg[1] + (turn * 1) == msg[3])
	{
		index = index_in_board(msg[2], msg[3]);
		if (this->_player == 0 && (isupper(board[index]) == true || board[index] == '#'))
		{
			return false;
		}
		else if (this->_player == 0 && isupper(board[index]) != true)
		{
			return true;
		}
		else if (this->_player == 1 && isupper(board[index]) != true)
		{
			return false;
		}
		else if (this->_player == 1 && isupper(board[index]) == true)
		{
			return true;
		}
	}

	return ans;
}
