#include "Bishop.h"

Bishop::Bishop(int player, char type) : BasePiece(player, type)
{

}

Bishop::~Bishop()
{
}

bool Bishop::good_place(std::string msg, std::string board)
{
	bool ans = false;
	bool to_check = true;
	char dst_letter = msg[2];
	int to_put;

	char src_letter = msg[0];
	int src_num = int(msg[1]) - 49 + 1;
	int ascii_src_num = msg[1];

	//right part
	if (src_letter != 'h')
	{
		//part one-> top right
		if (msg[0] < msg[2] && msg[1] < msg[3])
		{
			while (src_letter != 'z')
			{
				src_letter++;
				src_num++;
				ascii_src_num++;
				to_put = index_in_board(src_letter, src_num);
				if (to_put < 0 || to_put > 63)
				{
					return false;
				}
				to_check = by_player(to_put, msg[3], ascii_src_num, board);
				if (to_check == false)
				{
					return false;
				}
				if (src_letter == msg[2] && ascii_src_num == msg[3])
				{
					return true;
				}
				if (src_letter == dst_letter)
				{
					src_letter = 'z';
				}
			}
		}
		//part 2-> bottom right
		if (msg[0] < msg[2] && msg[1] > msg[3])
		{
			to_check = true;
			src_letter = msg[0];
			src_num = int(msg[1]) - 49 + 1;
			ascii_src_num = msg[1];
			while (src_letter != 'z')
			{
				src_letter++;
				src_num--;
				ascii_src_num--;
				to_put = index_in_board(src_letter, src_num);
				if (to_put < 0 || to_put > 63)
				{
					return false;
				}
				to_check = by_player(to_put, msg[3], ascii_src_num, board);
				if (to_check == false)
				{
					return false;
				}
				if (src_letter == msg[2] && ascii_src_num == msg[3])
				{
					return true;
				}
				if (src_letter == dst_letter)
				{
					src_letter = 'z';
				}
			}
		}
	}
	to_check = true;
	src_letter = msg[0];
	src_num = int(msg[1]) - 49 + 1;
	ascii_src_num = msg[1];
	if (src_letter != 'a')
	{
		if (msg[0] > msg[2] && msg[1] < msg[3])
		{
			//part 3-> top left
			while (src_letter != 'z')
			{
				src_letter--;
				src_num++;
				ascii_src_num++;
				to_put = index_in_board(src_letter, src_num);
				if (to_put < 0 || to_put > 63)
				{
					return false;
				}
				to_check = by_player(to_put, msg[3], ascii_src_num, board);
				if (to_check == false)
				{
					return false;
				}
				if (src_letter == msg[2] && ascii_src_num == msg[3])
				{
					return true;
				}
				if (src_letter == dst_letter)
				{
					src_letter = 'z';
				}
			}
		}

		if (msg[0] > msg[2] && msg[1] > msg[3])
		{
			to_check = true;
			src_letter = msg[0];
			src_num = int(msg[1]) - 49 + 1;
			ascii_src_num = msg[1];
			while (src_letter != 'z')
			{
				src_letter--;
				src_num--;
				ascii_src_num--;
				to_put = index_in_board(src_letter, src_num);
				if (to_put < 0 || to_put > 63)
				{
					return false;
				}
				to_check = by_player(to_put, msg[3], ascii_src_num, board);
				if (to_check == false)
				{
					return false;
				}
				if (src_letter == msg[2] && ascii_src_num == msg[3])
				{
					return true;
				}
				if (src_letter == dst_letter)
				{
					src_letter = 'z';
				}
			}
		}
	}
	return ans;
}