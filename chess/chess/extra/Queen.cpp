#include "Queen.h"

Queen::Queen(int player, char type) : BasePiece(player, type)
{
}

Queen::~Queen()
{
}


bool Queen::good_place(std::string msg, std::string board)
{
	
	if (this->_player == 0)
	{
		return good_for_white(msg, board);
	}
	else
	{
		return good_for_black(msg, board);
		
	}
}


bool Queen::good_for_black(std::string msg, std::string board)
{
	Bishop b = Bishop(1, 'b');
	Rook r = Rook(1, 'r');

	if (b.good_place(msg, board) == true || r.good_place(msg, board) == true)
	{
		return true;
	}
	return false;
}

bool Queen::good_for_white(std::string msg, std::string board)
{
	Bishop b = Bishop(0, 'B');
	Rook r = Rook(0, 'R');

	if (b.good_place(msg, board) == true || r.good_place(msg, board) == true)
	{
		return true;
	}
	return false;
}
